#!/bin/bash

# Function to extract the CMD instruction for the specified stage
function extract_cmd_for_stage() {
  local stage="$1"
  local in_target_stage=false
  local dockerfile=$(find $CI_PROJECT_DIR -type f -name "Dockerfile" | head -n 1)

  while IFS= read -r line; do
    if [[ $line =~ ^FROM\ base\ as\ $stage ]]; then
      in_target_stage=true
      continue
    fi

    if [ "$in_target_stage" = true ]; then
      if [[ $line =~ ^CMD ]]; then
        local cmd_instruction="${line#CMD}"
        cmd_instruction="${cmd_instruction#"${cmd_instruction%%[![:space:]]*}"}"  # Trim leading spaces
        echo "$cmd_instruction"
        break
      fi
    fi
  done < $dockerfile
}

function configure() {
  export REPO_NAME=$(echo $CI_PROJECT_URL | sed 's#.*/##') # retain the part after the last slash

  if [[ -z "$CI_PROJECT_TITLE" ]]; then
    export CI_PROJECT_TITLE=$REPO_NAME
  fi

  export CONTAINER_NAME=$CI_PROJECT_TITLE-$TARGET_DIRECTORY

  if [[ -z "$CI_COMMIT_REF_NAME" ]]; then
    export CI_COMMIT_REF_NAME=$(git branch | sed -n -e 's/^\* \(.*\)/\1/p')
  fi

  if [[ "$CI_COMMIT_REF_NAME" == "development" ]]; then
    export CODENAME="devl"
    export RELEASE="non-production"
    export ENVIRONMENT="development"
  elif [[ "$CI_COMMIT_REF_NAME" == "staging" ]]; then
    export CODENAME="stag"
    export RELEASE="non-production"
    export ENVIRONMENT="staging"
  elif [[ "$CI_COMMIT_REF_NAME" == @(uat|canary) ]]; then
    export CODENAME="canr"
    export RELEASE="canary"
    export ENVIRONMENT="canary"
  elif [[ "$CI_COMMIT_REF_NAME" == @(main|master) ]]; then
    export CODENAME="prod"
    export RELEASE="production"
    export ENVIRONMENT="production"
  else
    printf >&2 "error: CI_COMMIT_REF_NAME '%s' did not match any branch(s) known to CI \n" $CI_COMMIT_REF_NAME
    exit 1
  fi

  if [[ -z "$DOCKERHUB_USERNAME" ]]; then
    export DOCKERHUB_USERNAME="gibranbadrul"
  fi

  export CONTAINER_TAG=$(git rev-parse --short HEAD)

  export CONTAINER_PORT=$(cat ${CI_PROJECT_DIR}/${TARGET_DIRECTORY}/Dockerfile | grep EXPOSE | head -1 | sed 's/[[:alpha:]|(|[:space:]]//g' | awk -F- '{print $1}');
  if [ -z $CONTAINER_PORT ]; then
    printf "Default container port not defined. Start generating random port...\n";
    export CONTAINER_PORT=$(shuf -i 1024-9999 -n 1);
  fi;

  s_extract_cmd_for_stage=$(extract_cmd_for_stage "$RELEASE")
  s_extract_cmd_for_stage=$(echo "$s_extract_cmd_for_stage" | tr -d '[],' | tr -d '"')

  export CONTAINER_ARGS="$s_extract_cmd_for_stage"
}

function make_artifacts() {
  # print config.vars to file as metadata
  printf "\n=============================================================\nMETADATA\n============================================================="
  printf "\n[+] CONTAINER_OWNER: %s" "$DOCKERHUB_USERNAME"
  printf "\n[+] CONTAINER_NAME: %s" "$CONTAINER_NAME"
  printf "\n[+] CONTAINER_TAG: %s-%s" "$CODENAME" "$CONTAINER_TAG"
  printf "\n[+] CONTAINER_PORT: %s" "$CONTAINER_PORT"
  printf "\n[+] CONTAINER_ARGS: %s" "$CONTAINER_ARGS"
  printf "\n[+] ENVIRONMENT: %s" "$ENVIRONMENT"
  printf "\n[+] RELEASE: %s" "$RELEASE"
  printf "\n[+] CODENAME: %s" "$CODENAME"
  printf "\n[+] TARGET_DIRECTORY: %s" "$TARGET_DIRECTORY"
  printf "\n=============================================================\n\n"

  j2 templates/metadata.tmpl > metadata

  printf >&2 "Building Docker images from feature branch and make commit hash as a tag...\n"
  make ci-artifact
}

function init() {
  cp templates/metadata.tmpl metadata

  configure "$@"
  make_artifacts "$@"
}

init "${@:-.}"
