include .ci/metadata

ifeq ($(UNAME),Darwin)
    SHELL   := /opt/local/bin/bash
    OS_X    := true
else ifneq (,$(wildcard /etc/redhat-release))
    OS_RHEL := true
else
    OS_DEB  := true
    SHELL   := /bin/bash
endif

# supported platform or arch:
# linux/amd64, linux/arm64, linux/riscv64, linux/ppc64le, linux/s390x, linux/386, linux/arm/v7, linux/arm/v6

test:
	@printf "\nImage: %s/%s:%s" ${CONTAINER_OWNER} ${CONTAINER_NAME} ${CONTAINER_TAG}
	@printf "\nRelease: %s\n" ${RELEASE}
	@printf "\nDirName: %s\nHash: %s\n" $(notdir $(shell pwd)) $(shell openssl rand -hex 3)

artifact:
	@docker buildx build \
		--cache-from type=registry,ref=gibranbadrul/$(notdir $(shell pwd)):cache \
		--cache-to type=registry,ref=gibranbadrul/$(notdir $(shell pwd)):cache,mode=max \
        --target non-production \
        --tag gibranbadrul/$(notdir $(shell pwd)):alpha-$(shell openssl rand -hex 3) \
        --platform linux/amd64 \
        --push .

.PHONY: context-dump ci-artifact

context-dump:
	@printf "\n=============================================================\nBUILD CONTEXT\n============================================================="
	@printf "\n[+] Image: %s/%s:%s" ${CONTAINER_OWNER} ${CONTAINER_NAME} ${CONTAINER_TAG}
	@printf "\n[+] Release: %s" ${RELEASE}
	@printf "\n=============================================================\n\n"

ci-artifact: context-dump
	@
	@cd $(shell pwd)/${TARGET_DIRECTORY} \
	 && docker buildx build \
		  --cache-from type=registry,ref=${CONTAINER_OWNER}/${CONTAINER_NAME}:cache \
		  --cache-to type=registry,ref=${CONTAINER_OWNER}/${CONTAINER_NAME}:cache,mode=max \
          --target ${RELEASE} \
          --tag ${CONTAINER_OWNER}/${CONTAINER_NAME}:${CONTAINER_TAG} \
          --platform linux/amd64 \
          --push .