# Pintu-monorepo

This repository hosts the "pintu-monorepo" application, featuring backend services developed in both GoLang and Node.js. As a monorepo, it streamlines version control and management for both backend components. Additionally, Kubernetes configurations are provided for efficient container orchestration.

## Table of Contents
- [Pintu-monorepo](#pintu-monorepo)
  - [Table of Contents](#table-of-contents)
- [Backend Services](#backend-services)
  - [GoLang Backend](#golang-backend)
  - [Node.js Backend](#nodejs-backend)
- [Directory Structure](#directory-structure)
- [Deep Dives](#deep-dives)
  - [The Approach](#the-approach)
  - [Gitlab (CI)](#gitlab-ci)
  - [DAG](#dag)
  - [Referencing job scripts](#referencing-job-scripts)
  - [Docker Build](#docker-build)
  - [Kubernetes Manifests](#kubernetes-manifests)
  - [Scenario: API Testing for CRUD Operations](#scenario-api-testing-for-crud-operations)
    - [Create Operation](#create-operation)
    - [Read Operation](#read-operation)
    - [Update Operation](#update-operation)
    - [List Operation](#list-operation)
    - [Delete Operation](#delete-operation)

# Backend Services

## GoLang Backend
The GoLang backend forms the core of the "pintu-monorepo" application, providing essential functionalities and business logic. It exposes RESTful APIs, enabling seamless interaction with clients and handling crucial data processing tasks.

* Implements robust business logic and data processing.
* Offers RESTful APIs for client interaction.
* Source code resides in the be-golang directory.

## Node.js Backend
The Node.js backend complements the GoLang backend, providing additional functionalities and services. It also exposes RESTful APIs, facilitating smooth communication with clients and integrating seamlessly with other components.

* Implements supplementary functionalities and services.
* Offers RESTful APIs for client interaction.
* Source code housed in the be-node directory.

# Directory Structure

```
.
├── .ci
│   ├── Makefile
│   ├── docker-build.sh
│   ├── templates
│   │   └── metadata.tmpl
│   └── workflows
│       ├── be-golang.yml
│       ├── be-node.yml
│       └── collections.yml
├── be-golang
│   ├── Dockerfile
│   ├── go.mod
│   ├── main.go
│   └── main_test.go
├── be-node
│   ├── .eslintrc.js
│   ├── Dockerfile
│   ├── app.js
│   ├── eslint.config.mjs
│   ├── package-lock.json
│   ├── package.json
│   └── test
│       └── app.test.js
├── .editorconfig
├── .gitignore
├── .gitlab-ci.yml
├── Makefile
└── README.md
```

# Deep Dives

## The Approach

The project is split into folders. For distinguishing if a given part of the project was modified, I use `rules:changes` so every part of the project that I want to be able to run in separation from the rest should be placed in one folder.

My assumptions are as follow:

> CI only runs on parts that have undergone changes — mainly to save CI resources. So I can avoid running a job for tens of minutes because of changes I know won't impact the job. This is especially important if I have code in repos that does not depend on each other *(e.g. our golang and nodejs applications are in different folders)*.

## Gitlab (CI)

The main focus of `CI` is only to a few files below

```
.
├── .ci
│   ├── Makefile
│   ├── docker-build.sh
│   ├── templates
│   │   └── metadata.tmpl
│   └── workflows
│       ├── be-golang.yml
│       ├── be-node.yml
│       └── collections.yml
├── .gitlab-ci.yml
└── Makefile
```

First, I will use the `include:` feature to separate each pipeline in to separate files.

[Stages still need to be defined](https://gitlab.com/gitlab-org/gitlab/-/issues/30632) but technically do not affect execution if `needs:` is specified on every job.

```yml
# .gitlab-ci.yml

image: gibranbadrul/docker-buildx

services:
  - docker:dind

variables:
  DOCKER_DRIVER: overlay2

stages:
  - test
  - build
  - manifest

include:
  - local: .ci/workflows/collections.yml
  - local: .ci/workflows/be-golang.yml
  - local: .ci/workflows/be-node.yml
```

## DAG

GitLab could have implemented multiple pipelines per repository any number of ways. Interestingly, GitLab decided that DAGs provided the most flexible way of implementing multiple pipelines per repository.

To build a Directed Acyclic Graph, add the `needs:` keyword to jobs in the pipeline.

> `be-golang` pipeline has a very similar looking `.ci/workflows/be-node.yml`.

```yml
---
# .ci/workflows/be-node.yml

test:node:
  stage: test
  image: node:18.20.3-alpine3.20
  script:
    - cd be-node
    - npm install --save-dev mocha chai supertest && npm test
  rules:
    - if: $CI_COMMIT_REF_NAME =~ '/development|staging|canary|uat|main|master/'
      changes:
        - be-node/**

test:node-lint:
  stage: test
  image: node:18.20.3-alpine3.20
  script:
    - cd be-node
    - npm install eslint --save-dev && npm run lint-fix
  rules:
    - if: $CI_COMMIT_REF_NAME =~ '/development|staging|canary|uat|main|master/'
      changes:
        - be-node/**

artifacts:node:
  stage: build
  image: gibranbadrul/docker-buildx
  rules:
    - if: $CI_COMMIT_REF_NAME =~ '/development|staging|canary|uat|main|master/'
      changes:
        - be-node/**
  script:
    - export TARGET_DIRECTORY="be-node"
    - !reference [.run_docker_buildx, script]
  artifacts:
    paths:
      - .ci/metadata
  needs:
    - test:node
    - test:node-lint

manifest:node:
  stage: manifest
  image: gibranbadrul/git:2.45.1-alpine
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      when: manual
      changes:
        - be-node/**
  script:
    - export SUFFIX_MANIFEST="node"
    - !reference [.git_config, script]
    - !reference [.git_manifest_clone, script]
    - !reference [.git_generate_tmpl_prod, script]
    - !reference [.git_pre_commit, script]
    - !reference [.git_push_commit, script]
  needs:
    - artifacts:node

```

The point is that even though all the components of `be-node` and `be-golang` look similar, every `artifacts:*` job will only consider the success/failure of the 2 jobs listed in its `needs:`.

Triggering the pipeline shows that each component is deploying indepenently:

![gitlab_dag](assets/img-3.png)

When only a change occurs in one of them:

![gitlab_dag](assets/img-4.png)

> The `test`/`build`/`manifest` stages have run properly in both backends.

## Referencing job scripts

```yml
...
  script:
    - export SUFFIX_MANIFEST="node"
    - !reference [.git_config, script]
    - !reference [.git_manifest_clone, script]
    - !reference [.git_generate_tmpl_prod, script]
    - !reference [.git_pre_commit, script]
    - !reference [.git_push_commit, script]
```

And I also use the `!reference` feature to reuse the defined scripts from the `collections.yml` files. The syntax for referencing job parts `!reference [referenced_job, referenced_keyword_section]`.

```yml
---
# .ci/workflows/collections.yml

.export_metadata: &export_metadata
  script:
    - . <(sed -E -n '/^\s*[[:alpha:]_][[:alnum:]_]*=/ s/^/export /p' < .ci/metadata)

.git_config: &git_config
  script:
    - |
      git config --global credential.helper store \
      && GIT_TOKENS=$(echo $GIT_TOKENS | base64 -d)
    - |
      git credential approve << EOF
      protocol=https
      host=$CI_SERVER_FQDN
      username=$GIT_USERNAME
      password=$GIT_TOKENS
      EOF

.git_manifest_clone: &git_manifest_clone
  script:
    - |
      git clone https://$GIT_USERNAME:$GIT_TOKENS@$CI_SERVER_FQDN/$CI_PROJECT_PATH-$SUFFIX_MANIFEST-manifest.git \
        && export MANIFEST_DIR=$CI_PROJECT_NAME-$SUFFIX_MANIFEST-manifest

.git_generate_tmpl_nonprod: &git_generate_tmpl_nonprod
  script:
    - !reference [.export_metadata, script]
    - cp .ci/metadata $MANIFEST_DIR/$RELEASE/$ENVIRONMENT/.metadata
    - j2 $MANIFEST_DIR/.ci/templates/docker-compose.yml > $MANIFEST_DIR/$RELEASE/$ENVIRONMENT/docker-compose.yml

.git_generate_tmpl_prod: &git_generate_tmpl_prod
  script:
    - !reference [.export_metadata, script]
    - j2 $MANIFEST_DIR/templates/base/deployment.yml > $MANIFEST_DIR/base/deployment.yml
    - j2 $MANIFEST_DIR/templates/base/service.yml > $MANIFEST_DIR/base/service.yml
    - j2 $MANIFEST_DIR/templates/overlays/deployment.yml > $MANIFEST_DIR/overlays/$ENVIRONMENT/deployment.yml
    - j2 $MANIFEST_DIR/templates/overlays/kustomization.yml > $MANIFEST_DIR/overlays/$ENVIRONMENT/kustomization.yml
    - j2 $MANIFEST_DIR/templates/overlays/version.yml > $MANIFEST_DIR/overlays/$ENVIRONMENT/version.yml

.git_pre_commit: &git_pre_commit
  script:
    - |
      cd $MANIFEST_DIR \
        && git remote set-url origin "https://${GIT_USERNAME}:${GIT_TOKENS}@${CI_SERVER_FQDN}/${CI_PROJECT_PATH}-${SUFFIX_MANIFEST}-manifest.git" \
        && git config --global push.default simple \
        && git config --global user.name "$GIT_USERNAME" \
        && git config --global user.email "$GIT_EMAIL" \
        && git add .

.git_push_commit: &git_push_commit
  script:
    - |
      if [[ $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH ]]; then
        git commit -m "ci: bump production image to $CONTAINER_TAG via $CI_PIPELINE_URL";
      else
        exit 1
      fi;
    - git push

.run_docker_buildx: &run_docker_buildx
  script:
    - |
      export DOCKER_BUILDKIT=1 \
        && echo $DOCKERHUB_PASSWORD | base64 -d | docker login --username gibranbadrul --password-stdin \
        && docker run --rm --privileged multiarch/qemu-user-static --reset -p yes \
        && docker buildx create --name multiarch --use \
        && cd ${CI_PROJECT_DIR}/.ci \
        && bash docker-build.sh
```

## Docker Build

Let's dive into how the `artifact:*` job work. This process is powered by some handy shell scripts to streamline our CI pipeline in our different environments, like development, staging, and production, etc.

```sh
#!/bin/bash

# docker-build.sh

# Function to extract the CMD instruction for the specified stage
function extract_cmd_for_stage() {
  local stage="$1"
  local in_target_stage=false
  local dockerfile=$(find $CI_PROJECT_DIR -type f -name "Dockerfile" | head -n 1)

  while IFS= read -r line; do
    if [[ $line =~ ^FROM\ base\ as\ $stage ]]; then
      in_target_stage=true
      continue
    fi

    if [ "$in_target_stage" = true ]; then
      if [[ $line =~ ^CMD ]]; then
        local cmd_instruction="${line#CMD}"
        cmd_instruction="${cmd_instruction#"${cmd_instruction%%[![:space:]]*}"}"  # Trim leading spaces
        echo "$cmd_instruction"
        break
      fi
    fi
  done < $dockerfile
}

function configure() {
  export REPO_NAME=$(echo $CI_PROJECT_URL | sed 's#.*/##') # retain the part after the last slash

  if [[ -z "$CI_PROJECT_TITLE" ]]; then
    export CI_PROJECT_TITLE=$REPO_NAME
  fi

  export CONTAINER_NAME=$CI_PROJECT_TITLE-$TARGET_DIRECTORY

  if [[ -z "$CI_COMMIT_REF_NAME" ]]; then
    export CI_COMMIT_REF_NAME=$(git branch | sed -n -e 's/^\* \(.*\)/\1/p')
  fi

  if [[ "$CI_COMMIT_REF_NAME" == "development" ]]; then
    export CODENAME="devl"
    export RELEASE="non-production"
    export ENVIRONMENT="development"
  elif [[ "$CI_COMMIT_REF_NAME" == "staging" ]]; then
    export CODENAME="stag"
    export RELEASE="non-production"
    export ENVIRONMENT="staging"
  elif [[ "$CI_COMMIT_REF_NAME" == @(uat|canary) ]]; then
    export CODENAME="canr"
    export RELEASE="canary"
    export ENVIRONMENT="canary"
  elif [[ "$CI_COMMIT_REF_NAME" == @(main|master) ]]; then
    export CODENAME="prod"
    export RELEASE="production"
    export ENVIRONMENT="production"
  else
    printf >&2 "error: CI_COMMIT_REF_NAME '%s' did not match any branch(s) known to CI \n" $CI_COMMIT_REF_NAME
    exit 1
  fi

  if [[ -z "$DOCKERHUB_USERNAME" ]]; then
    export DOCKERHUB_USERNAME="gibranbadrul"
  fi

  export CONTAINER_TAG=$(git rev-parse --short HEAD)

  export CONTAINER_PORT=$(cat ${CI_PROJECT_DIR}/${TARGET_DIRECTORY}/Dockerfile | grep EXPOSE | head -1 | sed 's/[[:alpha:]|(|[:space:]]//g' | awk -F- '{print $1}');
  if [ -z $CONTAINER_PORT ]; then
    printf "Default container port not defined. Start generating random port...\n";
    export CONTAINER_PORT=$(shuf -i 1024-9999 -n 1);
  fi;

  s_extract_cmd_for_stage=$(extract_cmd_for_stage "$RELEASE")
  s_extract_cmd_for_stage=$(echo "$s_extract_cmd_for_stage" | tr -d '[],' | tr -d '"')

  export CONTAINER_ARGS="$s_extract_cmd_for_stage"
}

function make_artifacts() {
  # print config.vars to file as metadata
  printf "\n=============================================================\nMETADATA\n============================================================="
  printf "\n[+] CONTAINER_OWNER: %s" "$DOCKERHUB_USERNAME"
  printf "\n[+] CONTAINER_NAME: %s" "$CONTAINER_NAME"
  printf "\n[+] CONTAINER_TAG: %s-%s" "$CODENAME" "$CONTAINER_TAG"
  printf "\n[+] CONTAINER_PORT: %s" "$CONTAINER_PORT"
  printf "\n[+] CONTAINER_ARGS: %s" "$CONTAINER_ARGS"
  printf "\n[+] ENVIRONMENT: %s" "$ENVIRONMENT"
  printf "\n[+] RELEASE: %s" "$RELEASE"
  printf "\n[+] CODENAME: %s" "$CODENAME"
  printf "\n[+] TARGET_DIRECTORY: %s" "$TARGET_DIRECTORY"
  printf "\n=============================================================\n\n"

  j2 templates/metadata.tmpl > metadata

  printf >&2 "Building Docker images from feature branch and make commit hash as a tag...\n"
  make ci-artifact
}

function init() {
  cp templates/metadata.tmpl metadata

  configure "$@"
  make_artifacts "$@"
}

init "${@:-.}"
```

```makefile
# Makefile

include .ci/metadata

ifeq ($(UNAME),Darwin)
    SHELL   := /opt/local/bin/bash
    OS_X    := true
else ifneq (,$(wildcard /etc/redhat-release))
    OS_RHEL := true
else
    OS_DEB  := true
    SHELL   := /bin/bash
endif

# supported platform or arch:
# linux/amd64, linux/arm64, linux/riscv64, linux/ppc64le, linux/s390x, linux/386, linux/arm/v7, linux/arm/v6

.PHONY: context-dump ci-artifact

context-dump:
	@printf "\n=============================================================\nBUILD CONTEXT\n============================================================="
	@printf "\n[+] Image: %s/%s:%s" ${CONTAINER_OWNER} ${CONTAINER_NAME} ${CONTAINER_TAG}
	@printf "\n[+] Release: %s" ${RELEASE}
	@printf "\n=============================================================\n\n"

ci-artifact: context-dump
	@cd $(shell pwd)/${TARGET_DIRECTORY} \
	 && docker buildx build \
		  --cache-from type=registry,ref=${CONTAINER_OWNER}/${CONTAINER_NAME}:cache \
		  --cache-to type=registry,ref=${CONTAINER_OWNER}/${CONTAINER_NAME}:cache,mode=max \
          --target ${RELEASE} \
          --tag ${CONTAINER_OWNER}/${CONTAINER_NAME}:${CONTAINER_TAG} \
          --platform linux/amd64 \
          --push .
```

Here’s a quick look at what the `docker-build.sh` script does:

* **Setting Up Environment Variables**: The script kicks off by setting up some crucial environment variables. These include the repository name, the branch or tag name, and the target directory. These variables help customize the build process based on the specific context of the commit or tag.
* **Conditional Logic**: Depending on the branch or tag that triggered the build, the script adjusts its behavior. For instance, a build from the development branch might have different settings compared to one from a production tag. This ensures that each build is correctly configured for its environment.
* **Generating Metadata**: Collects all the relevant details like the container tag, container name, and commit hash, etc, and stores them in a structured format. This metadata is essential for the next stage.
* **Running Docker Commands**: Through the `make_artifacts()` function, the script execute the Docker commands to construct the image, ensuring that each step is informed by the comprehensive metadata, resulting in precise image creation. And I also use Docker Buildx with `docker cache` feature enabled to speed up the build process and allow for multi-platform builds, ensuring our images work on various architectures if needed.

The complete metada template files are like this:

```env
CONTAINER_OWNER={{ DOCKERHUB_USERNAME }}
CONTAINER_NAME={{ CONTAINER_NAME }}
CONTAINER_TAG={{ CODENAME }}-{{ CONTAINER_TAG }}
CONTAINER_PORT={{ CONTAINER_PORT }}
CONTAINER_ARGS="{{ CONTAINER_ARGS }}"

ENVIRONMENT={{ ENVIRONMENT }}
RELEASE={{ RELEASE }}
CODENAME={{ CODENAME }}
TARGET_DIRECTORY={{ TARGET_DIRECTORY }}
```

When the `artifacts` job is underway:

![alt text](assets/img-6.png)

## Kubernetes Manifests

The Kubernetes manifest process involves collecting data from the metadata to render templates stored in the manifest repository. The manifest repository is separate from the monorepo project and is structured as microrepositories based on backends application within the monorepo project *(e.g. `be-node` and `be-golang`)*.

Here are the links:

* [Node.js Manifest Repository](https://gitlab.com/gibransvarga/pintu-monorepo-node-manifest.git)
* [Golang Manifest Repository](https://gitlab.com/gibransvarga/pintu-monorepo-golang-manifest.git)

When the `manifest` job is underway, it will be like this:

![alt text](assets/img-7.png)

As proof that the manifest was successfully created..

![alt text](assets/img-8.png)

Meanwhile, for the deployment process, I utilize `ArgoCD` to deploy `kustomize` manifests to the Kubernetes cluster.

![alt text](assets/img-9.png)

## Scenario: API Testing for CRUD Operations

| Operation  | Method | GoLang Endpoint                  | Node.js Endpoint                  |
|------------|--------|----------------------------------|-----------------------------------|
| Create     | POST   | http://go.192.168.59.101.nip.io/create     | http://node.192.168.59.101.nip.io/create      |
| Read       | GET    | http://go.192.168.59.101.nip.io/read       | http://node.192.168.59.101.nip.io/read        |
| Update     | PUT    | http://go.192.168.59.101.nip.io/update     | http://node.192.168.59.101.nip.io/update      |
| Delete     | DELETE | http://go.192.168.59.101.nip.io/delete     | http://node.192.168.59.101.nip.io/delete      |
| List       | GET    | http://go.192.168.59.101.nip.io/list       | http://node.192.168.59.101.nip.io/list        |


### Create Operation

Send a POST request to /create endpoint with a JSON body containing the item name.

```sh
curl -X POST -H "Content-Type: application/json" -d '{"name":"Android"}' http://go.192.168.59.101.nip.io/create
```
<details>
  <summary>Click for Go Results</summary>
  ![alt text](assets/api-golang-1.png)
</details>

```bash
curl -X POST -H "Content-Type: application/json" -d '{"name":"Apple"}' http://node.192.168.59.101.nip.io/create
```

<details>
  <summary>Click for Node Results</summary>
  ![alt text](assets/api-node-1.png)
</details>

### Read Operation

Send a GET request to /read endpoint with an optional query parameter id to retrieve a specific item by its ID.

```bash
curl http://go.192.168.59.101.nip.io/read?id=3
```

<details>
  <summary>Click for Go Results</summary>
  ![alt text](assets/api-golang-2.png)
</details>

```bash
curl http://node.192.168.59.101.nip.io/read?id=3
```

<details>
  <summary>Click for Node Results</summary>
  ![alt text](assets/api-node-2.png)
</details>

### Update Operation

Send a PUT request to /update endpoint with query parameter id and a JSON body containing the updated item name.

```bash
curl -X PUT -H "Content-Type: application/json" -d '{"name":"Bitcoin"}' http://go.192.168.59.101.nip.io/update?id=3
```

<details>
  <summary>Click for Go Results</summary>
  ![alt text](assets/api-golang-3.png)
</details>

```bash
curl -X PUT -H "Content-Type: application/json" -d '{"name":"Jackfruit"}' http://node.192.168.59.101.nip.io/update?id=3
```

<details>
  <summary>Click for Node Results</summary>
  ![alt text](assets/api-node-3.png)
</details>

### List Operation

Send a GET request to /list endpoint to retrieve a list of all items.

```bash
curl http://go.192.168.59.101.nip.io/list
```

<details>
  <summary>Click for Go Results</summary>
  ![alt text](assets/api-golang-4.png)
</details>

```bash
curl http://node.192.168.59.101.nip.io/list
```

<details>
  <summary>Click for Node Results</summary>
  ![alt text](assets/api-node-4.png)
</details>

### Delete Operation

Send a DELETE request to /delete endpoint with query parameter id to delete a specific item by its ID.

```bash
curl -X DELETE http://go.192.168.59.101.nip.io/delete?id=2
```

<details>
  <summary>Click for Go Results</summary>
  ![alt text](assets/api-golang-5.png)
</details>

```bash
curl -X DELETE http://node.192.168.59.101.nip.io/delete?id=1
```

<details>
  <summary>Click for Node Results</summary>
  ![alt text](assets/api-node-5.png)
</details>