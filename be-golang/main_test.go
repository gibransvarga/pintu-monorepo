package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestCRUD(t *testing.T) {
	t.Run("Create", func(t *testing.T) {
		newItem := Item{Name: "New Item"}
		reqBody, _ := json.Marshal(newItem)
		req, _ := http.NewRequest("POST", "/create", bytes.NewBuffer(reqBody))
		rr := httptest.NewRecorder()
		createHandler(rr, req)

		if rr.Code != http.StatusOK {
			t.Errorf("Create: expected status 200, got %d", rr.Code)
		}
	})

	t.Run("Read", func(t *testing.T) {
		req, _ := http.NewRequest("GET", "/read?id=1", nil)
		rr := httptest.NewRecorder()
		readHandler(rr, req)

		if rr.Code != http.StatusOK {
			t.Errorf("Read: expected status 200, got %d", rr.Code)
		}
	})

	t.Run("Update", func(t *testing.T) {
		updatedItem := Item{Name: "Updated Item"}
		reqBody, _ := json.Marshal(updatedItem)
		req, _ := http.NewRequest("PUT", "/update?id=1", bytes.NewBuffer(reqBody))
		rr := httptest.NewRecorder()
		updateHandler(rr, req)

		if rr.Code != http.StatusOK {
			t.Errorf("Update: expected status 200, got %d", rr.Code)
		}
	})

	t.Run("Delete", func(t *testing.T) {
		// Simulate the scenario where the item does not exist
		// by sending a DELETE request for an item that does not exist.
		req, _ := http.NewRequest("DELETE", "/delete?id=999", nil)
		rr := httptest.NewRecorder()
		deleteHandler(rr, req)

		// Expect a status code of 404 because the item does not exist.
		if rr.Code != http.StatusNotFound {
			t.Errorf("Delete: expected status 404, got %d", rr.Code)
		}
	})

	t.Run("List all items", func(t *testing.T) {
		req, _ := http.NewRequest("GET", "/list", nil)
		rr := httptest.NewRecorder()
		listHandler(rr, req)

		if rr.Code != http.StatusOK {
			t.Errorf("List all items: expected status 200, got %d", rr.Code)
		}
	})
}
