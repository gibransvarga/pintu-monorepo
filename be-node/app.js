const express = require("express");
const app = express();
const port = 3000;

app.use(express.json());

let items = [];
let idCounter = 0;

app.post("/create", (req, res) => {
  const newItem = { id: ++idCounter, name: req.body.name };
  items.push(newItem);
  res.json(newItem);
});

app.get("/read", (req, res) => {
  const id = parseInt(req.query.id);
  if (id) {
    const item = items.find((item) => item.id === id);
    if (!item) {
      res.status(404).json({ error: "Item not found" });
    } else {
      res.json(item);
    }
  } else {
    res.json(items);
  }
});

app.put("/update", (req, res) => {
  const id = parseInt(req.query.id);
  if (!id) {
    res.status(400).json({ error: "ID required" });
    return;
  }

  const updatedItem = { id, name: req.body.name };
  const index = items.findIndex((item) => item.id === id);
  if (index === -1) {
    res.status(404).json({ error: "Item not found" });
  } else {
    items[index] = updatedItem;
    res.json(updatedItem);
  }
});

app.delete("/delete", (req, res) => {
  const id = parseInt(req.query.id);
  if (!id) {
    res.status(400).json({ error: "ID required" });
    return;
  }

  const index = items.findIndex((item) => item.id === id);
  if (index === -1) {
    res.status(404).json({ error: "Item not found" });
  } else {
    items.splice(index, 1);
    res.send(`Item with ID ${id} deleted`);
  }
});

app.get("/list", (req, res) => {
  res.json(items);
});

app.listen(port, () => {
  console.log(`Node.js backend running at http://localhost:${port}`);
});

module.exports = app;
