const request = require('supertest');
const app = require('../app');
const { describe, before, it } = require('mocha');
const { expect } = require('chai');

describe('CRUD operations', () => {
  let newItemId;

  before(async () => {
    // Create the item with ID 1 and name "Updated Item"
    const response = await request(app)
      .post('/create')
      .send({ name: 'Updated Item' });
    expect(response.status).to.equal(200);
    newItemId = response.body.id;
  });

  it('Read', async () => {
    const response = await request(app).get(`/read?id=${newItemId}`);
    expect(response.status).to.equal(200);
    expect(response.body.name).to.equal('Updated Item');
  });

  it('Update', async () => {
    const response = await request(app)
      .put(`/update?id=${newItemId}`)
      .send({ name: 'Updated Item' });
    expect(response.status).to.equal(200);
    expect(response.body.name).to.equal('Updated Item');
  });

  it('Delete', async () => {
    const response = await request(app).delete(`/delete?id=${newItemId}`);
    expect(response.status).to.equal(200);
    expect(response.text).to.equal(`Item with ID ${newItemId} deleted`);
  });

  it('List all items', async () => {
    const response = await request(app).get('/list');
    expect(response.status).to.equal(200);
    expect(response.body).to.have.lengthOf(0);
  });
});

